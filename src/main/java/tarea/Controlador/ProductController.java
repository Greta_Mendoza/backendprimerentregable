package tarea.Controlador;

import tarea.Models.ProductModel;
import tarea.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("${api.version}/productos")
public class ProductController {

    @Autowired
    private ProductService productoService;

    // GET todos los productos
    @GetMapping
    public ResponseEntity getProduct() {
       return ResponseEntity.ok(this.productoService.getProduct());
    }

    @PostMapping
    public ResponseEntity crearProducto(@RequestBody ProductModel producto){
        final ProductModel p = this.productoService.addProduct(producto);
        return ResponseEntity.ok(p);
    }

    @GetMapping("/{id}")
    public ResponseEntity obtenerUnProducto(@PathVariable int id){
        final ProductModel p = this.productoService.getProductID(id);
        if (p != null) {
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity borrarUnProducto(@PathVariable int id){
        this.productoService.deleteProduct(id);
        return new ResponseEntity("¡Eliminado!", HttpStatus.NO_CONTENT);
    }

}
