package tarea.Controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tarea.Models.ProductModel;
import tarea.Models.UserModel;
import tarea.Service.ProductService;

@RestController
@RequestMapping("${api.version}/productos")
public class UserProductController {

    @Autowired
    private ProductService productoService;

    @GetMapping("/{idProducto}/usuarios")
    public ResponseEntity obtenerUsuarioProducto(@PathVariable int idProducto){
        try{
            return ResponseEntity.ok(this.productoService.getUsersProduct(idProducto));
        }catch (Exception x){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("/{idProducto}/usuarios/{idUsario}")
    public ResponseEntity obtenerUsuarioProducto(@PathVariable int idProducto, @PathVariable int idUsuario){
        final UserModel u = this.productoService.getUserProduct(idProducto, idUsuario);
        return (u == null)
                ? new ResponseEntity(HttpStatus.NOT_FOUND)
                :ResponseEntity.ok(u);
    }

    @PostMapping("/{idProducto}/usuarios")
    public ResponseEntity agregarUsuarioProducto(@PathVariable int idProducto, @RequestBody UserModel usuario){
        final ProductModel p = this.productoService.getProductID(idProducto);
        if (p == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        return ResponseEntity.ok(this.productoService.addUserProduct(idProducto,usuario));
    }

    @DeleteMapping("/{idProducto}/usuarios/{idUsuario}")
    public ResponseEntity borrarUsuarioProducto (@PathVariable int idProducto, @PathVariable int idUsuario){
        this.productoService.deleteUserProduct(idProducto, idUsuario);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
