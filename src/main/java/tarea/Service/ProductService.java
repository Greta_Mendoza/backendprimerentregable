package tarea.Service;
import tarea.Models.ProductModel;
import tarea.Models.UserModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class ProductService {

    private final AtomicInteger secuenciaIds = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdUsuario = new AtomicInteger( 0);
    private final List<ProductModel> productos = new ArrayList<ProductModel>();

    // Create
    public ProductModel addProduct(ProductModel producto){
        producto.setId(this.secuenciaIds.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }

    public UserModel addUserProduct(int idProducto, UserModel usuario){
        usuario.setuserId(this.secuenciaIdUsuario.incrementAndGet());
        this.getProductID(idProducto).getUsers().add(usuario);
       return usuario;
    }

    // Read todos los productos
    public List<ProductModel> getProduct(){
        return Collections.unmodifiableList(this.productos);
    }

    // Read todos los Usuarios
    public List<UserModel> getUsersProduct(int idProducto){
        return Collections.unmodifiableList(this.getProductID(idProducto).getUsers());
    }

    // Read elemento
    public ProductModel getProductID(int id){
        for(ProductModel p: this.productos){
            if(p.getId() == id)
                return p;
        }
        return null;
    }

    public UserModel getUserProduct(int idProducto, int idUsuario){
        for(UserModel u: this.getProductID(idProducto).getUsers()){
            if(u.getuserId() == idUsuario)
                return u;
        }
        return null;
    }


    // Actualiza Producto --UPDATE
    public boolean updateProduct(int id, ProductModel producto){
        for(ProductModel p: this.productos){
         if(p.getId() == id){
             p.setMarca(producto.getMarca());
             p.setDescripcion(producto.getDescripcion());
             p.setPrecio(producto.getPrecio());
             //p.setUsers(List.copyOf(producto.getUsers()));
             return true;
         }
        }
        return false;
    }

    // Borra productos --DELETE
    public boolean deleteProduct(int id){
        for(int i = 0; i < this.productos.size(); ++i){
            if(this.productos.get(i).getId() == id){
                this.productos.remove(i);
                return true;
            }
        }
        return false;
    }

    public boolean deleteUserProduct(int idProducto, int idUser){
        final ProductModel producto = this.getProductID(idProducto);
        if(producto == null)
            return false;
        final List<UserModel> usuarios = producto.getUsers();
        for(int i = 0; i < usuarios.size(); ++i){
            if(usuarios.get(i).getuserId() == idUser){
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }

}
