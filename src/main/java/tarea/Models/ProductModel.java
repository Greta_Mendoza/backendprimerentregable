package tarea.Models;

import java.util.List;

public class ProductModel {
    private int id;
    private String marca;
    private String descripcion;
    private double precio;
    private List<UserModel> users;

    public ProductModel(int id, String marca, String descripcion, double precio, List<UserModel> users) {
        this.id = id;
        this.marca = marca;
        this.descripcion = descripcion;
        this.precio = precio;
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    //public void setUsers(List<UserModel> users) {
      //  this.users = users;
    //}
}
