package tarea.Models;

public class UserModel {
    private int userId;
    private String nombre;

    public UserModel(){}

    public UserModel(int userId, String nombre){
        this.userId = userId;
        this.nombre = nombre;
    }

    public void setNombre (String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return this.nombre;
    }

    public void setuserId (int userId){
        this.userId = userId;
    }

    public int getuserId(){
        return this.userId;
    }

}
